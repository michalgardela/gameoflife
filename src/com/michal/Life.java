package com.michal;

import java.util.Random;

public class Life {
	// tablica z komorkami
	protected boolean tab[][] = null;
	protected int width = 0;
	protected int height = 0;

	// zasady gry
	protected int TO_REBORN[] = { 3 };
	protected int TO_ALIVE[] = { 2, 3 };

	public Life(int width, int height, double probability) {
		if (width > 0 && height > 0) {
			this.width = width;
			this.height = height;
			System.out.print("Prawdow  " + probability);
			this.tab = new boolean[this.width][this.height];

			Random rand = new Random();

			for (int x = 0; x < this.width; x++) {
				for (int y = 0; y < this.height; y++) {
					if (rand.nextDouble() < probability) {
						this.tab[x][y] = true;
					} else {
						this.tab[x][y] = false;
					}
				}
			}
		} else {
			throw new NegativeArraySizeException();
		}
	}

	public Life(boolean tab[][]) {
		this.width = tab.length;
		this.height = tab[0].length;

		if (this.width > 0 && this.height > 0) {
			this.tab = tab;
		} else {
			throw new NegativeArraySizeException();
		}
	}

	public boolean[][] iterate() {
		boolean tempTab[][] = new boolean[this.width][this.height];

		for (int x = 0; x < this.width; x++) {
			for (int y = 0; y < this.height; y++) {
				// zywa - sprawdzamy czy ma umrzec
				if (this.tab[x][y] == true) {
					for (int i : this.TO_ALIVE) {
						tempTab[x][y] = false;

						if (i == this.neighborsNr(x, y)) {
							// zyje dalej
							tempTab[x][y] = true;
							break;
						}

						// inaczej umiera
					}
				}
				// martwa - sprawdzamy czy ma sie odrodzic
				else {
					for (int i : this.TO_REBORN) {
						tempTab[x][y] = false;

						if (i == this.neighborsNr(x, y)) {
							// ozywa
							tempTab[x][y] = true;
							break;
						}

						// inaczej dalej nie zyje
					}
				}
			}
		}

		this.tab = tempTab;

		return this.tab;
	}

	public boolean[][] getWorld() {
		return this.tab;
	}

	protected int neighborsNr(int x, int y) {
		if (x >= this.width || x < 0 || y >= this.height || y < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}

		int neighbors = 0;

		if (this.isLife(x - 1, y - 1)) {
			neighbors++;
		}

		if (this.isLife(x - 1, y)) {
			neighbors++;
		}

		if (this.isLife(x - 1, y + 1)) {
			neighbors++;
		}

		if (this.isLife(x, y - 1)) {
			neighbors++;
		}

		if (this.isLife(x, y + 1)) {
			neighbors++;
		}

		if (this.isLife(x + 1, y - 1)) {
			neighbors++;
		}

		if (this.isLife(x + 1, y)) {
			neighbors++;
		}

		if (this.isLife(x + 1, y + 1)) {
			neighbors++;
		}

		return neighbors;
	}

	protected boolean inBounds(int x, int y) {
		if (x >= this.width || x < 0 || y >= this.height || y < 0) {
			return false;
		} else {
			return true;
		}
	}

	protected boolean isLife(int x, int y) {
		if (this.inBounds(x, y) == true) {
			if (this.tab[x][y] == true) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
