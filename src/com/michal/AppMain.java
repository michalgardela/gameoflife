package com.michal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

public class AppMain {

	private static JFrame frmGameOfLife;
	private GCanvas canvas;
	private JPanel panel;
	private JLabel lblTitle;
	Life world;
	final int canvasSize = 500;
	volatile int boxSize = 10;
	volatile int n = canvasSize / boxSize;
	volatile int fps = 15;
	volatile double fillRatio = 0.4;
	private JSlider sliderFPS;
	private JLabel lblSpeed;
	private JToggleButton tglbtnPlay;
	private JButton btnRestart;
	private JSlider sliderSize;
	private JLabel lblSize;
	private JSlider sliderFill;
	private Updater updater;
	private JTextField tfSpeed;
	private JTextField tfSize;
	private JTextField tfFill;
	private static final String version = "1.2a";
	private static volatile boolean isAlive = true;

	private JLabel lblFill;
	private Component rigidArea;
	private Component rigidArea_1;
	private Component rigidArea_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
					new AppMain();
					frmGameOfLife.setLocationRelativeTo(null);
					frmGameOfLife.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		Thread sleepAccuracyThread = new Thread() {
			@Override
			public void run() {
				while (isAlive) {
					try {
						Thread.sleep(Long.MAX_VALUE);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		// sleepAccuracyThread.setDaemon(true);
		sleepAccuracyThread.start();
	}

	/**
	 * Create the application.
	 */
	public AppMain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		frmGameOfLife = new JFrame();
		frmGameOfLife.setTitle("Game of Life v" + version);
		frmGameOfLife.getContentPane().setEnabled(false);
		frmGameOfLife.setResizable(false);
		frmGameOfLife.setSize(820, 565);
		frmGameOfLife.setBounds(100, 100, 820, 546);
		frmGameOfLife.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGameOfLife.setBackground(Color.lightGray);
		GridBagConstraints gbc_canvas_1 = new GridBagConstraints();
		gbc_canvas_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_canvas_1.insets = new Insets(0, 0, 0, 5);
		gbc_canvas_1.gridx = 0;
		gbc_canvas_1.gridy = 0;
		frmGameOfLife.getContentPane().setLayout(new MigLayout("", "[502.00px][280.00px]", "[502.00px]"));

		canvas = new GCanvas();
		canvas.boxSize = boxSize;
		canvas.setIgnoreRepaint(true);
		// canvas = new JPanel();
		canvas.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		canvas.setSize(500, 500);
		canvas.setBounds(0, 0, 500, 500);
		canvas.setForeground(Color.BLACK);
		canvas.setFont(null);
		canvas.setBackground(new Color(192, 192, 192));
		frmGameOfLife.getContentPane().add(canvas, "cell 0 0,grow");

		panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setSize(500, 500);
		panel.setOpaque(true);
		frmGameOfLife.getContentPane().add(panel, "cell 1 0,grow");
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 66, 70, 142 };
		gbl_panel.rowHeights = new int[] { 28, 30, 0, 0, 0, 30, 28, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 1.0, 0.0 };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		rigidArea_2 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_2 = new GridBagConstraints();
		gbc_rigidArea_2.fill = GridBagConstraints.BOTH;
		gbc_rigidArea_2.gridwidth = 3;
		gbc_rigidArea_2.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea_2.gridx = 0;
		gbc_rigidArea_2.gridy = 0;
		panel.add(rigidArea_2, gbc_rigidArea_2);

		lblTitle = new JLabel("<html><b style=\"text-align: center;\">Game of Life v" + version
				+ " - Michał Gardeła 2013</center></b></html>");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblTitle = new GridBagConstraints();
		gbc_lblTitle.fill = GridBagConstraints.BOTH;
		gbc_lblTitle.gridwidth = 3;
		gbc_lblTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblTitle.gridx = 0;
		gbc_lblTitle.gridy = 1;
		panel.add(lblTitle, gbc_lblTitle);

		rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.fill = GridBagConstraints.BOTH;
		gbc_rigidArea.gridwidth = 3;
		gbc_rigidArea.insets = new Insets(0, 0, 5, 0);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 2;
		panel.add(rigidArea, gbc_rigidArea);

		lblSpeed = new JLabel("Speed");
		GridBagConstraints gbc_lblSpeed = new GridBagConstraints();
		gbc_lblSpeed.anchor = GridBagConstraints.EAST;
		gbc_lblSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpeed.gridx = 0;
		gbc_lblSpeed.gridy = 3;
		panel.add(lblSpeed, gbc_lblSpeed);

		tfSpeed = new JTextField();
		tfSpeed.setEditable(false);
		tfSpeed.setText("" + fps);
		GridBagConstraints gbc_tfSpeed = new GridBagConstraints();
		gbc_tfSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_tfSpeed.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSpeed.gridx = 1;
		gbc_tfSpeed.gridy = 3;
		panel.add(tfSpeed, gbc_tfSpeed);
		tfSpeed.setColumns(10);

		sliderFPS = new JSlider();
		sliderFPS.setMaximum(300);
		sliderFPS.setMinimum(1);
		sliderFPS.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (sliderFPS.getValueIsAdjusting()) {
					fps = sliderFPS.getValue();
					tfSpeed.setText("" + sliderFPS.getValue());
				}
			}
		});
		sliderFPS.setValue(fps);
		sliderFPS.setPaintLabels(true);
		GridBagConstraints gbc_sliderFPS = new GridBagConstraints();
		gbc_sliderFPS.insets = new Insets(0, 0, 5, 0);
		gbc_sliderFPS.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderFPS.gridx = 2;
		gbc_sliderFPS.gridy = 3;
		panel.add(sliderFPS, gbc_sliderFPS);

		lblSize = new JLabel("Grid Size");
		GridBagConstraints gbc_lblSize = new GridBagConstraints();
		gbc_lblSize.anchor = GridBagConstraints.EAST;
		gbc_lblSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblSize.gridx = 0;
		gbc_lblSize.gridy = 4;
		panel.add(lblSize, gbc_lblSize);

		tfSize = new JTextField();
		tfSize.setEditable(false);
		tfSize.setText(n + "");
		GridBagConstraints gbc_tfSize = new GridBagConstraints();
		gbc_tfSize.insets = new Insets(0, 0, 5, 5);
		gbc_tfSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSize.gridx = 1;
		gbc_tfSize.gridy = 4;
		panel.add(tfSize, gbc_tfSize);
		tfSize.setColumns(10);

		sliderSize = new JSlider();
		sliderSize.setMaximum(250);
		sliderSize.setValue(n);
		sliderSize.setMinimum(25);
		sliderSize.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (sliderSize.getValueIsAdjusting()) {
					n = sliderSize.getValue();
					tfSize.setText(n + "");
					boxSize = (int) Math.ceil(canvasSize / (double) n);
					System.out.println("BoxSize: " + boxSize + " n: " + n);
				}
			}
		});

		GridBagConstraints gbc_sliderSize = new GridBagConstraints();
		gbc_sliderSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderSize.insets = new Insets(0, 0, 5, 0);
		gbc_sliderSize.gridx = 2;
		gbc_sliderSize.gridy = 4;
		panel.add(sliderSize, gbc_sliderSize);

		lblFill = new JLabel("Fill Ratio");
		GridBagConstraints gbc_lblFill = new GridBagConstraints();
		gbc_lblFill.insets = new Insets(0, 0, 5, 5);
		gbc_lblFill.anchor = GridBagConstraints.EAST;
		gbc_lblFill.gridx = 0;
		gbc_lblFill.gridy = 5;
		panel.add(lblFill, gbc_lblFill);

		sliderFill = new JSlider();
		sliderFill.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (sliderFill.getValueIsAdjusting()) {
					fillRatio = sliderFill.getValue() / 100f;
					tfFill.setText(String.format("%.2f", fillRatio));
				}
			}
		});

		tfFill = new JTextField();
		tfFill.setEditable(false);
		tfFill.setText("" + fillRatio);
		GridBagConstraints gbc_tfFill = new GridBagConstraints();
		gbc_tfFill.insets = new Insets(0, 0, 5, 5);
		gbc_tfFill.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfFill.gridx = 1;
		gbc_tfFill.gridy = 5;
		panel.add(tfFill, gbc_tfFill);
		tfFill.setColumns(10);

		GridBagConstraints gbc_sliderFill = new GridBagConstraints();
		gbc_sliderFill.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderFill.insets = new Insets(0, 0, 5, 0);
		gbc_sliderFill.gridx = 2;
		gbc_sliderFill.gridy = 5;
		panel.add(sliderFill, gbc_sliderFill);

		world = new Life(n, n, 0.5f);

		updater = new Updater();

		updater.start();

		btnRestart = new JButton("Restart");
		btnRestart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				boolean pause = updater.paused;
				updater.paused = true;
				canvas.boxSize = boxSize;
				world = new Life(n, n, fillRatio);
				canvas.z = n;
				canvas.repaint();
				updater.paused = pause;

			}
		});

		tglbtnPlay = new JToggleButton("Play");
		tglbtnPlay.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (tglbtnPlay.isSelected()) {
					updater.paused = false;
				} else {
					updater.paused = true;
				}
			}
		});

		rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea_1 = new GridBagConstraints();
		gbc_rigidArea_1.fill = GridBagConstraints.BOTH;
		gbc_rigidArea_1.gridwidth = 3;
		gbc_rigidArea_1.insets = new Insets(0, 0, 5, 0);
		gbc_rigidArea_1.gridx = 0;
		gbc_rigidArea_1.gridy = 6;
		panel.add(rigidArea_1, gbc_rigidArea_1);

		GridBagConstraints gbc_tglbtnPlay = new GridBagConstraints();
		gbc_tglbtnPlay.gridwidth = 2;
		gbc_tglbtnPlay.insets = new Insets(0, 0, 5, 5);
		gbc_tglbtnPlay.gridx = 0;
		gbc_tglbtnPlay.gridy = 7;
		panel.add(tglbtnPlay, gbc_tglbtnPlay);

		GridBagConstraints gbc_btnRestart = new GridBagConstraints();
		gbc_btnRestart.insets = new Insets(0, 0, 5, 0);
		gbc_btnRestart.gridx = 2;
		gbc_btnRestart.gridy = 7;
		panel.add(btnRestart, gbc_btnRestart);

	}

	class Updater extends Thread {

		public volatile boolean running = true;
		public volatile boolean paused = true;

		@Override
		public void run() {
			while (running) {
				if (paused) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					if (paused) {
						continue;
					}
				}

				world.iterate();
				canvas.repaint();

				try {
					Thread.sleep((long) (1e3 / fps));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	class GCanvas extends JPanel {

		private static final long serialVersionUID = 1L;

		volatile int z = n;
		volatile int boxSize;

		@Override
		public synchronized void paintComponent(Graphics g) { /* this method must be overridden! */
			g.clearRect(0, 0, canvasSize + 10, canvasSize + 10);

			for (int c = 0; c < z; c++) {
				for (int r = 0; r < z; ++r) {

					if (world.getWorld()[c][r]) {

						g.setColor(Color.red);
						g.fillRect(c * boxSize, r * boxSize, boxSize, boxSize);

						g.setColor(Color.yellow);
						g.fillRect(c * boxSize, r * boxSize, boxSize - 1, boxSize - 1);

						g.setColor(Color.orange);
						g.fillRect(c * boxSize + 1, r * boxSize + 1, boxSize - 2, boxSize - 2);

					}

				}
			}
		}

		@Override
		public synchronized void repaint() {
			super.repaint();
		}
	}

}
